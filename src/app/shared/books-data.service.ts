import { Book } from './book';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BooksDataService {

  booksURL = 'http://milenabooks.azurewebsites.net/api/Books';
  pokemons: Book[];
  count: number = 0;
  offset: number = 0;
  limit: number = 20;

  constructor(private http: HttpClient) { }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.booksURL)
  }

  
}

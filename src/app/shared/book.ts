export interface Book {
    id: number;
    name: string;
    price: number;
    author: string;
    url: string;
    rate: number;
}
